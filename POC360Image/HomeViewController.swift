//
//  HomeViewController.swift
//  POC360Image
//
//  Created by pvharsha on 2/1/17.
//  Copyright © 2017 pvharsha. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {
    
    enum Media {
        static var photoArray = ["sphdd.jpg","sphdd2.jpg","sindhu_beach.jpg", "grand_canyon.jpg", "underwater.jpg"]
//                                 "sphdd.jpg","sphdd2.jpg","sindhu_beach.jpg", "grand_canyon.jpg", "underwater.jpg",
//                                 "sphdd.jpg","sphdd2.jpg","sindhu_beach.jpg", "grand_canyon.jpg", "underwater.jpg",
//                                 "sphdd.jpg","sphdd2.jpg","sindhu_beach.jpg", "grand_canyon.jpg", "underwater.jpg"]
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "360 Image Viewer"
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Media.photoArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...
        let imageVRView1: GVRPanoramaView! = cell.viewWithTag(1001) as! GVRPanoramaView;
        imageVRView1.load(UIImage(named: Media.photoArray[indexPath.row]), of: GVRPanoramaImageType.mono)
        imageVRView1.isUserInteractionEnabled = false
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewControllerObj:ViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as! ViewController
        viewControllerObj.imageData = UIImage(named: Media.photoArray[indexPath.row])
        self.navigationController?.pushViewController(viewControllerObj, animated: true)
    }
    
}
