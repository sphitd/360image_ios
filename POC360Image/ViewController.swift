//
//  ViewController.swift
//  POC360Image
//
//  Created by pvharsha on 1/31/17.
//  Copyright © 2017 pvharsha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imageVRView1: GVRPanoramaView!
    var imageData:UIImage!
    
    var currentView: UIView?
    var currentDisplayMode = GVRWidgetDisplayMode.embedded
    var isPaused = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageVRView1.isHidden = true
        
//        self.imageVRView1.load(UIImage(named: Media.photoArray.first!), of: GVRPanoramaImageType.mono)
        self.imageVRView1.load(imageData, of: GVRPanoramaImageType.mono)
//        self.imageVRView1.enableCardboardButton = true
        self.imageVRView1.enableFullscreenButton = true
        self.imageVRView1.displayMode = GVRWidgetDisplayMode.fullscreen
        self.imageVRView1.delegate = self
    }
    
    func setCurrentViewFromTouch(touchPoint point:CGPoint) {
        if self.imageVRView1!.frame.contains(point) {
            currentView = self.imageVRView1
        }
    }
}

extension ViewController: GVRWidgetViewDelegate {
    func widgetView(_ widgetView: GVRWidgetView!, didLoadContent content: Any!) {
        if content is UIImage {
            self.imageVRView1.isHidden = false
        }
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didFailToLoadContent content: Any!, withErrorMessage errorMessage: String!)  {
        print(errorMessage)
    }
    
    func widgetView(_ widgetView: GVRWidgetView!, didChange displayMode: GVRWidgetDisplayMode) {
        _ = self.navigationController?.popToRootViewController(animated: true)
        /*currentView = widgetView
        currentDisplayMode = displayMode
        if currentView == self.imageVRView1 && currentDisplayMode != GVRWidgetDisplayMode.embedded {
            view.isHidden = true
        } else {
            view.isHidden = false
        }*/
    }
    
    func widgetViewDidTap(_ widgetView: GVRWidgetView!) {
        guard currentDisplayMode != GVRWidgetDisplayMode.embedded else {return}
        if currentView == self.imageVRView1 {
//            Media.photoArray.append(Media.photoArray.removeFirst())
//            self.imageVRView1?.load(UIImage(named: Media.photoArray.first!), of: GVRPanoramaImageType.mono)
            self.imageVRView1?.load(imageData, of: GVRPanoramaImageType.mono)
        }
    }
}

